## Kubernetes Task
***
#### Task
Set up Kubernetes v.1.14.4 cluster  
Vagrant nodes:  
    - kubernetes_master (2 CPU, 2048 RAM, OS: CentOS 7)  
    - kubernetes_slave1 (1 CPU, 1024 RAM , OS: CentOS 7)  
    - kubernetes_slave2 (1 CPU, 1024 RAM , OS: CentOS 7)  
    - kubernetes_slave3 (1 CPU, 1024 RAM , OS: CentOS 7)  
Create a separate namespace < name-surname >  
Repeat task [Deploying PHP Guestbook application with Redis](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/) in the created <name-surname> namespace.  
Upgrade Kubernetes cluster from v.1.14 to v1.15
***
#### Using Vagrantfile and scripts
Vagrantfile describes creating of four VMs with appropriate parameters: kubernetesmaster, kubernetesslave1, kubernetesslave2 and kubernetesslave3.  
Sycned folder was used to get data needed to create cluster via scripts.  

<details>
<summary>
<code>Vagrantfile</code>
</summary>
<p>

```ruby
Vagrant.configure("2") do |config|

  #Create nodes:

  config.vm.define "kubernetesmaster" do |kubernetesmaster|
    kubernetesmaster.vm.box = "centos/7"
    kubernetesmaster.vm.provision "shell", path: "some.sh"
    kubernetesmaster.vm.network "public_network"
    kubernetesmaster.vm.hostname = "kubernetesmaster"
    # Install vagrant-vbguest plugin before setuping synced folder.
    kubernetesmaster.vm.synced_folder "./vagrant", "/vagrant", type: "virtualbox"
    kubernetesmaster.vm.provider "virtualbox" do |vb|
         vb.memory = "2048"
         vb.cpus = "2"
     end
  end

(1..3).each do |i|
  config.vm.define "kubernetesslave#{i}" do |kubernetesslave|
    kubernetesslave.vm.box = "centos/7"
    kubernetesslave.vm.provision "shell", path: "somes.sh"
    kubernetesslave.vm.network "public_network"
    kubernetesslave.vm.hostname = "kubernetesslave#{i}"
    # Install vagrant-vbguest plugin before setuping synced folder.
    kubernetesslave.vm.synced_folder "./vagrant", "/vagrant", type: "virtualbox"
    kubernetesslave.vm.provider "virtualbox" do |vb|
         vb.memory = "1024"
         vb.cpus = "1"
     end
  end
 end
end
```
</p>
</details>

<details>
<summary>
<code><i>some.sh</i> script for kubernetesmaster</code>
</summary>
<p>

```bash
#!/bin/bash
#Update system.
yum update -y
#Remove NAT interface from using by default. So only bridge is used by default.
ip route delete default via 10.0.2.2 dev eth0
# Install Docker CE
## Set up the repository
### Install required packages.
yum install yum-utils device-mapper-persistent-data lvm2 -y
### Add Docker repository.
yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo
## Install Docker CE.
#yum update
yum install docker-ce-18.06.2.ce -y
## Create /etc/docker directory.
mkdir /etc/docker
# Setup daemon.
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
## Create /etc/systemd/system/docker.service.d directory.
mkdir -p /etc/systemd/system/docker.service.d
# Restart Docker
systemctl daemon-reload
systemctl enable docker.service
systemctl restart docker
# Set following Kernel parameter as required by Kubernetes.
cat > /etc/sysctl.d/kubernetes.conf << EOF
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
# Reload Kernel parameter configuration files.
modprobe br_netfilter
sysctl --system
# Turn off Swap for Kubernetes installation.
swapoff -a
sed -e '/swap/s/^/#/g' -i /etc/fstab
# Allow Kubernetes service ports and Weave ports in Linux firewall.
systemctl enable firewalld
systemctl restart firewalld
firewall-cmd --permanent --add-port={6783,6443,2379,2380,10250,10251,10252,179}/tcp
firewall-cmd --permanent --add-port={6783,6784}/udp
firewall-cmd --reload
# Switch SELinux to Permissive mode using following commands.
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
# Add Kubernetes yum repository
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
# Install Kubernetes packages using yum command.
yum install -y kubelet-1.14.4 kubeadm-1.14.4 kubectl-1.14.4
# Execute the script provided by kubectl command itself to enable automatic completion of kubectl commands.
source <(kubectl completion bash)
# Add the script in Bash Completion directory.
kubectl completion bash > /etc/bash_completion.d/kubectl
# Pull Kebernetes config images.
kubeadm config images pull
# Save ip address to synced folder. We need it to use "kubeadm join" from nodes.
IP=$(hostname -I | awk '{print $2}') && echo $IP > /vagrant/ip.txt
# Start kubelet.
systemctl enable kubelet.service
systemctl start kubelet.service
# Initialize cluster.
kubeadm init --kubernetes-version $(kubeadm version -o short) --pod-network-cidr=192.168.0.0/16 --token-ttl 0
# Save token to synced folder to use it further.
kubeadm token list | awk 'NR == 2{print$1}' > /vagrant/token.txt
# Execute following commands as suggested by kubeadm init output command.
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
# Install network plugin (Weave).
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
# Create namespace yaroslav-chizh
kubectl create namespace yaroslav-chizh
# Set namespace yaroslav-chizh to use by default
kubectl config set-context --current --namespace=yaroslav-chizh
```
</p>
</details>

<details>
<summary>
<code><i>somes.sh</i> script for kubernetesslaves</code>
</summary>
<p>

```bash
#!/bin/bash
#Update system.
yum update -y
#Remove NAT interface from using by default. So only bridge is used by default.
ip route delete default via 10.0.2.2 dev eth0
# Install Docker CE
## Set up the repository
### Install required packages.
yum install yum-utils device-mapper-persistent-data lvm2 -y
### Add Docker repository.
yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo
## Install Docker CE.
#yum update
yum install docker-ce-18.06.2.ce -y
## Create /etc/docker directory.
mkdir /etc/docker
# Setup daemon.
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
## Create /etc/systemd/system/docker.service.d directory.
mkdir -p /etc/systemd/system/docker.service.d
# Restart Docker
systemctl daemon-reload
systemctl enable docker
systemctl restart docker
# Set following Kernel parameter as required by Kubernetes.
cat > /etc/sysctl.d/kubernetes.conf << EOF
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
# Reload Kernel parameter configuration files.
modprobe br_netfilter
sysctl --system
# Turn off Swap for Kubernetes installation.
swapoff -a
sed -e '/swap/s/^/#/g' -i /etc/fstab
# Allow Kubernetes service ports and Weave ports in Linux firewall.
sudo systemctl enable firewalld
sudo systemctl restart firewalld
firewall-cmd --permanent --add-port={10250,30000-32767}/tcp
firewall-cmd --reload
# Switch SELinux to Permissive mode using following commands.
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
# Add Kubernetes yum repository
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
# Install Kubernetes packages using yum command.
yum install -y kubeadm-1.14.4 kubelet-1.14.4  kubectl-1.14.4
# Execute the script provided by kubectl command itself to enable automatic completion of kubectl commands.
source <(kubectl completion bash)
# Add the script in Bash Completion directory.
kubectl completion bash > /etc/bash_completion.d/kubectl
# Start kubelet.
systemctl enable kubelet.service
systemctl start kubelet.service
# Use IP and TOKEN from synced folder.
IP=$(cat /vagrant/ip.txt)
TOKEN=$(cat /vagrant/token.txt)
# Join node to the cluster.
kubeadm join $IP:6443 --token $TOKEN --discovery-token-unsafe-skip-ca-verification
```
</p>
</details>

**Vagrant-vbguest** plugin was installed to set up synced folder.  
Synced folder is used to get IP address and token from master node.  
These parameters we use to automatize slave nodes connection.

Vagrantfile works awesome:

![](https://i.screenshot.net/5olj4c6)

Kebernetes does not work with latest Docker version, so here was used *docker-ce-18.06.2.ce.*    
As network plugin we use *Weave-net* one.  
We create required namespace **yaroslav-chizh** via:  
```kubectl create namespace yaroslav-chizh```  
And, set up namespace yaroslav-chizh to use by default via:  
```kubectl config set-context --current --namespace=yaroslav-chizh```

![](https://i.screenshot.net/er6g8hr)

We have working cluster just after ```vagrant up``` (I mean, just after around 40 minutes after ```vagrant up```).

![](https://i.screenshot.net/mz8w1tj)

***
#### Deploying PHP Guestbook application with Redis

##### Start up the Redis Master

- **Creating the Redis Master Deployment**

<details>
<summary>
<code>redis-master-deployment.yaml</code>
</summary>
<p>

```yaml
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: redis-master
  labels:
    app: redis
spec:
  selector:
    matchLabels:
      app: redis
      role: master
      tier: backend
  replicas: 1
  template:
    metadata:
      labels:
        app: redis
        role: master
        tier: backend
    spec:
      containers:
      - name: master
        image: k8s.gcr.io/redis:e2e  # or just image: redis
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 6379
```
</P>
</details>

Apply the Redis Master Deployment from the *redis-master-deployment.yaml* file:  
```kubectl apply -f https://k8s.io/examples/application/guestbook/redis-master-deployment.yaml```

We can see created *redis-master* pod and deployment.  
Also, on screenshot below we, could check that *yaroslav-chizh* namespace is used as default.

![](https://i.screenshot.net/8kvretx)

- **Creating the Redis Master Service**

<details>
<summary>
<code>redis-master-service.yaml</code>
</summary>

<p>

```yaml
apiVersion: v1
kind: Service
metadata:
  name: redis-master
  labels:
    app: redis
    role: master
    tier: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: redis
    role: master
    tier: backend
```    
</p>
</details>

Apply the Redis Master Service from the following *redis-master-service.yaml* file:  
```kubectl apply -f https://k8s.io/examples/application/guestbook/redis-master-service.yaml```

![](https://i.screenshot.net/q21k5a9)

##### Start up the Redis Slaves

- **Creating the Redis Slave Deployment**

<details>
<summary>
<code>redis-slave-deployment.yaml</code>
</summary>

<p>

```yaml
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: redis-slave
  labels:
    app: redis
spec:
  selector:
    matchLabels:
      app: redis
      role: slave
      tier: backend
  replicas: 2
  template:
    metadata:
      labels:
        app: redis
        role: slave
        tier: backend
    spec:
      containers:
      - name: slave
        image: gcr.io/google_samples/gb-redisslave:v3
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: GET_HOSTS_FROM
          value: dns
          # Using `GET_HOSTS_FROM=dns` requires your cluster to
          # provide a dns service. As of Kubernetes 1.3, DNS is a built-in
          # service launched automatically. However, if the cluster you are using
          # does not have a built-in DNS service, you can instead
          # access an environment variable to find the master
          # service's host. To do so, comment out the 'value: dns' line above, and
          # uncomment the line below:
          # value: env
        ports:
        - containerPort: 6379
```    
</p>
</details>

Apply the Redis Slave Deployment from the *redis-slave-deployment.yaml* file:  
```kubectl apply -f https://k8s.io/examples/application/guestbook/redis-slave-deployment.yaml```

![](https://i.screenshot.net/dvzonup)

- **Creating the Redis Slave Service**

<details>
<summary>
<code>redis-slave-service.yaml</code>
</summary>

<p>

```yaml
apiVersion: v1
kind: Service
metadata:
  name: redis-slave
  labels:
    app: redis
    role: slave
    tier: backend
spec:
  ports:
  - port: 6379
  selector:
    app: redis
    role: slave
    tier: backend
```
</p>
</details>    

Apply the Redis Slave Service from the following redis-slave-service.yaml file:  
```kubectl apply -f https://k8s.io/examples/application/guestbook/redis-slave-service.yaml```

![](https://i.screenshot.net/5n8lwcw)

##### Set up and Expose the Guestbook Frontend
- **Creating the Guestbook Frontend Deployment**

<details>
<summary>
<code>frontend-deployment.yaml</code>
</summary>
<p>

```yaml
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: frontend
  labels:
    app: guestbook
spec:
  selector:
    matchLabels:
      app: guestbook
      tier: frontend
  replicas: 3
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google-samples/gb-frontend:v4
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: GET_HOSTS_FROM
          value: dns
          # Using `GET_HOSTS_FROM=dns` requires your cluster to
          # provide a dns service. As of Kubernetes 1.3, DNS is a built-in
          # service launched automatically. However, if the cluster you are using
          # does not have a built-in DNS service, you can instead
          # access an environment variable to find the master
          # service's host. To do so, comment out the 'value: dns' line above, and
          # uncomment the line below:
          # value: env
        ports:
        - containerPort: 80
```
</p>
</details>        

Apply the frontend Deployment from the *frontend-deployment.yaml* file:   
```kubectl apply -f https://k8s.io/examples/application/guestbook/frontend-deployment.yaml```

![](https://i.screenshot.net/mxvpdt4)

- **Creating the Frontend Service**

<details>
<summary>
<code>frontend-service.yaml</code>
</summary>
<p>

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  # comment or delete the following line if you want to use a LoadBalancer
  type: NodePort
  # if your cluster supports it, uncomment the following to automatically create
  # an external load-balanced IP for the frontend service.
  # type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: guestbook
    tier: frontend
```
</p>
</details>

Apply the frontend Service from the *frontend-service.yaml* file:  
```kubectl apply -f https://k8s.io/examples/application/guestbook/frontend-service.yaml```

![](https://i.screenshot.net/53yd0sp)

Finally, we have got working app!

![](https://i.screenshot.net/wo6x4hr)
***

#### Upgrading kubeadm clusters from v1.14.4 to v1.15.5

##### Upgrading control plane node  
```yum install -y kubeadm-1.15.5-0 --disableexcludes=kubernetes```

![](https://i.screenshot.net/6m653se)

```sudo kubeadm upgrade plan```

![](https://i.screenshot.net/jj0l4fj)

<details>
<summary>
<code>sudo kubeadm upgrade apply v1.15.5</code>
</summary>

<p>

```
[root@kubernetesmaster ~]# sudo kubeadm upgrade apply v1.15.5
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[preflight] Running pre-flight checks.
[upgrade] Making sure the cluster is healthy:
[upgrade/version] You have chosen to change the cluster version to "v1.15.5"
[upgrade/versions] Cluster version: v1.14.4
[upgrade/versions] kubeadm version: v1.15.5
[upgrade/confirm] Are you sure you want to proceed with the upgrade? [y/N]: y
[upgrade/prepull] Will prepull images for components [kube-apiserver kube-controller-manager kube-scheduler etcd]
[upgrade/prepull] Prepulling image for component etcd.
[upgrade/prepull] Prepulling image for component kube-apiserver.
[upgrade/prepull] Prepulling image for component kube-controller-manager.
[upgrade/prepull] Prepulling image for component kube-scheduler.
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-etcd
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-controller-manager
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-apiserver
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-etcd
[apiclient] Found 1 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[upgrade/prepull] Prepulled image for component etcd.
[upgrade/prepull] Prepulled image for component kube-apiserver.
[upgrade/prepull] Prepulled image for component kube-scheduler.
[upgrade/prepull] Prepulled image for component kube-controller-manager.
[upgrade/prepull] Successfully prepulled the images for all the control plane components
[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.15.5"...
Static pod: kube-apiserver-kubernetesmaster hash: d5c083c0992ed276b413f8d918f760ba
Static pod: kube-controller-manager-kubernetesmaster hash: 9b57a2c48fbb8768cda08f949228e288
Static pod: kube-scheduler-kubernetesmaster hash: d892f5aa01870a63abd70166d7968a45
[upgrade/etcd] Upgrading to TLS for etcd
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests983822816"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2019-11-08-16-20-20/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-apiserver-kubernetesmaster hash: d5c083c0992ed276b413f8d918f760ba
Static pod: kube-apiserver-kubernetesmaster hash: 7111f2a1cb700343d06b9a7833f114de
[apiclient] Found 1 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2019-11-08-16-20-20/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-controller-manager-kubernetesmaster hash: 9b57a2c48fbb8768cda08f949228e288
Static pod: kube-controller-manager-kubernetesmaster hash: c7d99d452f7556b0892feeee197bc5c9
[apiclient] Found 1 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2019-11-08-16-20-20/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-scheduler-kubernetesmaster hash: d892f5aa01870a63abd70166d7968a45
Static pod: kube-scheduler-kubernetesmaster hash: c11be424dea16a6a843cc8cd1543373b
[apiclient] Found 1 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.15" in namespace kube-system with the configuration for the kubelets in the cluster
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.15" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.15.5". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.
```

</p>
</details>

##### Upgrade kubelet and kubectl on all control plane node

```yum install -y kubelet-1.15.5-0 kubectl-1.15.5-0 --disableexcludes=kubernetes```

![](https://i.screenshot.net/plm0ob6)

Restart the kubelet  
```sudo systemctl restart kubelet```

![](https://i.screenshot.net/p5nk7u2)

#### Upgrade worker nodes

##### Upgrade kubeadm on all worker nodes

kubernetesslave1:  
 ```yum install -y kubeadm-1.15.5-0 --disableexcludes=kubernetes```

 ![](https://i.screenshot.net/94960tw)

 ###### Drain the node

kubectl drain kubernetesslave1 --ignore-daemonsets

![](https://i.screenshot.net/6jkz2fo)

We can see that master node is upgraded now. Node kubernetesslave1 is inactive, but kubernetesslave2 and kubernetesslave3 are active. So our app is working.

![](https://i.screenshot.net/mqol7im)

```kubeadm upgrade node```

![](https://i.screenshot.net/d0mndi8)

Upgrade the kubelet and kubectl on worker node

```yum install -y kubelet-1.15.5-0 kubectl-1.15.5-0 --disableexcludes=kubernetes```

![](https://i.screenshot.net/vl1o4bn)

Restart the kubelet

```sudo systemctl restart kubelet```

![](https://i.screenshot.net/0527eu1)

Bring the node back online by marking it schedulable.

```kubectl uncordon kubernetesslave1```

![](https://i.screenshot.net/64yg4cn)

So now, just repeat the same for other nodes.  
Here is interesting point. On the screenshot above we can see pods running only on the second and third slave nodes, even after joining node back.  

![](https://i.screenshot.net/v9p19s0)

But, after upgrading kubernetesslave2 node we see pods running on first and third nodes.

![](https://i.screenshot.net/jv4jvbz)

And finally the whole cluster is upgraded.

We can scale up the number of frontend Pods to get all nodes in use:

![](https://i.screenshot.net/7m0ymsq)
***
##### That are useful links I've used during working on this task:
**[Installing kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)**  
**[Container runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)**  
**[Creating a single control-plane cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)**  
**[Integrating Kubernetes via the Addon](https://www.weave.works/docs/net/latest/kubernetes/kube-addon/)**  
**[Deploying PHP Guestbook application with Redis](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/)**  
**[Upgrading kubeadm clusters from v1.14 to v1.15](https://v1-15.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade-1-15/#upgrade-worker-nodes)**
***

# **I guess, all points of the Task were covered here.**
# **Thank you for your attention!**
